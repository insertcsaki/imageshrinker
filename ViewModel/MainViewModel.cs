﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Threading;
using ImageShrinker.DialogService;
using ImageShrinker.Miscellaneous;
using ImageShrinker.Model;

namespace ImageShrinker.ViewModel
{
    public class MainViewModel : ObservableObject
    {
        // Private fields
        private string _path;
        private ObservableCollection<ImageModel> _images = new ObservableCollection<ImageModel>();
        private ObservableCollection<ImageModel> _selectedImages = new ObservableCollection<ImageModel>();
        private readonly string[] _acceptableImageTypes = { "jpeg", "jpg", "png", "bmp" };
        private readonly ShrinkerService _shrinkerService;
        private readonly IDialogService _dialogService;
        private string _shrinkDestinationPath;
        private string _remainingPerFinished;
        private bool _canSetImageCollection = true;
        private bool _canStartShrinkingImages = true;
        private int _progressPercent;
        private int _quality;
        private string _statusLabel = "Ready to shrink.";

        // Commands
        public RelayCommand<string> ImageSetCommand { get; set; }
        public RelayCommand BrowseCommand { get; set; }
        public RelayCommand ExitCommand { get; set; }
        public RelayCommand ShrinkCommand { get; set; }
        public RelayCommand BrowseResizePathCommand { get; set; }
        public RelayCommand CancelCommand { get; set; }

        // Properties
        public int Quality
        {
            get { return _quality; }
            set
            {
                if (_quality == value) return;
                _quality = value;
                RaisePropertyChanged();
            }
        }

        public string StatusLabel
        {
            get { return _statusLabel; }
            set
            {
                if (_statusLabel == value) return;
                _statusLabel = value;
                RaisePropertyChanged();
            }
        }

        public string ShrinkDestinationPath
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_shrinkDestinationPath))
                    return Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Resized\\";
                return _shrinkDestinationPath;
            }
            set
            {
                if (_shrinkDestinationPath == value) return;
                _shrinkDestinationPath = value;
                RaisePropertyChanged();
            }
        }

        public int ProgressPercent
        {
            get { return _progressPercent; }
            set
            {
                if (_progressPercent == value) return;
                _progressPercent = value;
                RaisePropertyChanged();
            }
        }

        public string RemainingPerFinished
        {
            get { return _remainingPerFinished; }
            set
            {
                if (_remainingPerFinished == value) return;
                _remainingPerFinished = value;
                RaisePropertyChanged();
            }
        }

        private bool Cancelled { get; set; }

        private bool CanStartShrinkingImages
        {
            get { return _canStartShrinkingImages; }
            set
            {
                if (_canStartShrinkingImages == value) return;
                _canStartShrinkingImages = value;
                RaisePropertyChanged("CanUseButtons");
            }
        }

        private bool CanSetImageCollection
        {
            get { return _canSetImageCollection; }
            set
            {
                if (_canSetImageCollection == value) return;
                _canSetImageCollection = value;
                RaisePropertyChanged("CanUseButtons");
            }
        }

        public bool CanUseButtons
        {
            get { return CanStartShrinkingImages && CanSetImageCollection; }
        }

        public string PathImages
        {
            get { return _path; }
            set
            {
                if (_path == value) return;
                _path = value;
                RaisePropertyChanged();
                Images.Clear();
                ImageSetCommand.Execute(value);
            }
        }

        public ObservableCollection<ImageModel> Images
        {
            get
            {
                return _images;
            }
            set
            {
                if (_images == value) return;
                _images = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<ImageModel> SelectedImages
        {
            get { return _selectedImages; }
            set
            {
                _selectedImages = value; 
                RaisePropertyChanged();
            }
        }

        // Constructor
        public MainViewModel(IDialogService dialogservice, ShrinkerService shrinkerService)
        {
            _shrinkerService = shrinkerService;
            _dialogService = dialogservice;

            _shrinkerService.CleanupTempFolder();
            _shrinkerService.ReportProgressEvent.Subscribe(ReportShrinkingProgress);
            _shrinkerService.ReportCurrentImageEvent.Subscribe(currentImage 
                => UpdateStatusLabel(string.Format("Processing {0}...", currentImage)));

            BrowseCommand = new RelayCommand(SetPathImages);
            BrowseResizePathCommand = new RelayCommand(SetShrinkDestinationPath);
            ImageSetCommand = new RelayCommand<string>(SetImageCollection);
            ShrinkCommand = new RelayCommand(Shrink);
            CancelCommand = new RelayCommand(CancelAppropriately);
            ExitCommand = new RelayCommand((() => CloseWindowWithConfirmation(null, null)));
        }

        private void CancelAppropriately()
        {
            if (!CanStartShrinkingImages) CancelShrinking();
            else CancelAddingImages();
        }

        private void SetShrinkDestinationPath()
        {
            string selectedPath = _dialogService.OpenFolderBrowser();
            if (string.IsNullOrWhiteSpace(selectedPath)) return;
            ShrinkDestinationPath = selectedPath;
        }

        private void SetPathImages()
        {
            string selectedPath = _dialogService.OpenFolderBrowser();
            if (string.IsNullOrWhiteSpace(selectedPath)) return;
            PathImages = selectedPath;
        }


        // Methods
        private async void Shrink()
        {
            if (Images.Count == 0)
            {
                _dialogService.ShowErrorBox(@"No folder specified! Please select a folder using Browse function.", @"Error");
                return;
            }
            try
            {
                Cancelled = false;
                CanStartShrinkingImages = false;
                await _shrinkerService.ShrinkAllImagesAsync(Images, ShrinkDestinationPath, Quality);
                CanStartShrinkingImages = true;

                if (Cancelled) _dialogService.ShowInformationBox(@"Image shrinking has been cancelled!", @"Aborted");
                else _dialogService.ShowInformationBox(@"Successfully shrinked all pictures!.", @"Shrinking  finished");

                UpdateStatusLabel("Ready to shrink");
            }
            catch (Exception ex)
            {
                _dialogService.ShowErrorBox(@"An error occoured while shrinking. Error details: " + ex.Message, @"Error");
            }
            finally
            {
                UpdateStatusLabel("Ready to shrink.");
            }
        }

        private void SetImageCollection(string path)
        {
            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException("path");
            try
            {
                Cancelled = false;
                CanSetImageCollection = false;
                var pathList = Directory.EnumerateFiles(path).Where(IsImage).ToList();
                for (int i = 0; !Cancelled && i < pathList.Count; i++)
                {
                    var i2 = i;
                    Dispatcher.CurrentDispatcher.BeginInvoke(
                        new Action(() => UpdateStatusLabel(string.Format("Adding {0}...", new FileInfo(pathList[i2]).Name))),
                        DispatcherPriority.Background);

                    Dispatcher.CurrentDispatcher.BeginInvoke(
                        new Action(() => Images.Add(new ImageModel(pathList[i2], _shrinkerService))),
                        DispatcherPriority.Background);

                }
            }
            catch (Exception ex)
            {
                _dialogService.ShowErrorBox(
                    @"Error while filling up the app with shrinkable images! Error description:\n\r" + ex.Message, @"Error");
            }
            finally
            {
                Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() => UpdateStatusLabel("Ready to shrink.")),
                        DispatcherPriority.Background);
                CanSetImageCollection = true;

            }
        }

        private bool IsImage(string filePath)
        {
            return ContainsAny(new FileInfo(filePath).Extension.ToLower(), _acceptableImageTypes);
        }

        private static bool ContainsAny(string text, params string[] valuesToSearchFrom)
        {
            return valuesToSearchFrom.Any(text.Contains);
        }

        private void ReportShrinkingProgress(int remainingImages)
        {
            ProgressPercent = (int)((remainingImages / (double)Images.Count) * 100);
            RemainingPerFinished = remainingImages + "/" + Images.Count;
        }

        private void UpdateStatusLabel(string text)
        {
            StatusLabel = text;
        }

        private void CancelAddingImages()
        {
            Cancelled = true;
        }

        private void CancelShrinking()
        {
            Cancelled = true;
            _shrinkerService.StopShrinking();
            ProgressPercent = 0;
            RemainingPerFinished = "0/" + Images.Count;
        }

        private void Log(string message)
        {
            Trace.WriteLine(string.Format("[{0}] {1}", DateTime.Now.ToString("HH:mm:ss"), message));
        }

        internal void CloseWindowWithConfirmation(object sender, CancelEventArgs e)
        {
            if (!_dialogService.ShowYesNoQuestionBox(@"Are you sure you want to close the application?", @"Confirmation"))
            {
                e.Cancel = true;
                return;
            }
            Environment.Exit(0);
        }
    }
}
