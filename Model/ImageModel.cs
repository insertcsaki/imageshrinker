﻿using System.IO;
using ImageShrinker.Miscellaneous;

namespace ImageShrinker.Model
{
    public class ImageModel
    {
        public string Title { get; set; }
        public string OriginalPath { get; private set; }
        public string ThumbnailTempPath { get; set; }

        public ImageModel(string src, ShrinkerService shrinkerService)
        {
            OriginalPath = src;
            ThumbnailTempPath = shrinkerService.GetThumbnailFromTemp(src);
            Title = new FileInfo(src).Name;
        }

        public override string ToString()
        {
            return Title;
        }
    }
}
