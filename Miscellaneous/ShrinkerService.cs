﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Windows;
using ImageShrinker.Model;

namespace ImageShrinker.Miscellaneous
{
    public class ShrinkerService
    {
        // Private fields
        private DateTime _creationTime;
        private Dictionary<string, ImageCodecInfo> _encoders;
        private static readonly string _tempFolder = 
            Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\ImageResizer\thumbs";
        private bool CanShrink { get; set; }
        private Dictionary<string, ImageCodecInfo> Encoders
        {
            get
            {
                if (_encoders == null)
                {
                    _encoders = new Dictionary<string, ImageCodecInfo>();
                }
                if (_encoders.Count != 0) 
                    return _encoders;
                foreach (var codec in ImageCodecInfo.GetImageEncoders())
                {
                    _encoders.Add(codec.MimeType.ToLower(), codec);
                }
                return _encoders;
            }
        }

        // Events
        public readonly Event<int> ReportProgressEvent = new Event<int>();
        public readonly Event<string> ReportCurrentImageEvent = new Event<string>();

        // Methods
        public async Task ShrinkAllImagesAsync(ObservableCollection<ImageModel> images, string shrinkDestinationPath, int quality)
        {
            CanShrink = true;
            for (int i = 0; CanShrink && i < images.Count; i++)
            {
                ReportCurrentImageEvent.Raise(images[i].Title);
                await ShrinkAsync(images[i].OriginalPath, shrinkDestinationPath, quality);
                ReportProgressEvent.Raise(i + 1);
            }
        }

        private async Task ShrinkAsync(string filepath, string destinationPath, int quality)
        {
            try
            {
                if (!File.Exists(filepath)) throw new FileNotFoundException();
                using (var img = Image.FromFile(filepath))
                {
                    _creationTime = File.GetLastAccessTime(filepath);
                    string filename = Path.GetFileName(filepath);
                    CreateTempFolderIfNotExists();
                    string path = Path.Combine(destinationPath, filename);
                    await SaveImage(path, img, quality, img.PropertyItems);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(@"Exception handled!\n\n" + ex.Message);
            }
        }

        public void StopShrinking()
        {
            CanShrink = false;
        }

        private void WriteImageToFile(string fullPath, Bitmap image)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var fileStream = new FileStream(fullPath, FileMode.Create))
                {
                    image.Save(memoryStream, ImageFormat.Jpeg);
                    var bytes = memoryStream.ToArray();
                    fileStream.Write(bytes, 0, bytes.Length);
                }
            }
        }


        private async Task SaveImage(string path, Image image, int quality, PropertyItem[] properties)
        {
            ImageCodecInfo imgCodec;
            var qualityParam = new EncoderParameter(Encoder.Quality, quality);
            if (path.Substring(path.LastIndexOf('.') + 1).ToLower() == "jpg" ||
                path.Substring(path.LastIndexOf('.') + 1).ToLower() == "jpeg")
                imgCodec = GetEncoderInfo("image/jpeg");
            else imgCodec = GetEncoderInfo("image/png");

            var encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;

            for (int i = 0; i < properties.Length; i++)
            {
                image.PropertyItems[i] = properties[i];
            }
            await Task.Run(() => image.Save(path, imgCodec, encoderParams));
            File.SetCreationTime(path, _creationTime);
        }

        private ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            string lookupKey = mimeType.ToLower();
            ImageCodecInfo foundCodec = null;
            if (Encoders.ContainsKey(lookupKey))
            {
                foundCodec = Encoders[lookupKey];
            }
            return foundCodec;
        }


        public string GetThumbnailFromTemp(string path)
        {
            if (string.IsNullOrWhiteSpace(path)) throw new ArgumentNullException("path");
            var filename = Path.GetFileName(path);
            CreateTempFolderIfNotExists();
            var fullPath = _tempFolder + Path.DirectorySeparatorChar + filename;

            using (var bitmap = new Bitmap(Image.FromFile(path).GetThumbnailImage(100, 100, () => false, IntPtr.Zero)))
            {
                WriteImageToFile(fullPath, bitmap);
            }

            return fullPath;
        }
        private void CreateTempFolderIfNotExists()
        {
            if (Directory.Exists(_tempFolder)) return;
            Directory.CreateDirectory(_tempFolder);
            SetDirectoryAccess();
        }

        public void CleanupTempFolder()
        {
            if (!Directory.Exists(_tempFolder)) return;
            foreach (var file in new DirectoryInfo(_tempFolder).GetFiles())
            {
                file.Delete();
            }
        }

        private void SetDirectoryAccess()
        {
            var dInfo = new DirectoryInfo(_tempFolder);

            var dSecurity = dInfo.GetAccessControl();
            dSecurity.AddAccessRule(new FileSystemAccessRule(
                new SecurityIdentifier(
                    WellKnownSidType.BuiltinUsersSid,
                    null), FileSystemRights.DeleteSubdirectoriesAndFiles, //FileSystemRights.FullControl,
                AccessControlType.Allow));

            dInfo.SetAccessControl(dSecurity);
        }

    }

}
